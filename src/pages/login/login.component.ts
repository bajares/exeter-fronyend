import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from '../../providers/auth.service';

@Component({
  selector: './app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  constructor(
    private router: Router,
    private authService: AuthService
  ) {}

  ngOnInit() {
    if (window.localStorage.getItem('token')) {
      this.router.navigate(['/']);
    } else {
      this.setLoginForm();
    }
  }

  setLoginForm() {
    this.loginForm = new FormGroup({
      'email': new FormControl(
        null, [
          Validators.pattern('^([a-zA-Z0-9.-_]+)@([a-zA-Z0-9-_]+)\.([a-zA-Z]){2,5}$'),
          Validators.required
        ]),
      'password': new FormControl(null, [Validators.required, Validators.minLength(8)])
    });
  }

  onSubmit() {
    if (this.loginForm.valid) {

      this.authService.signin(this.loginForm)
        .then(result => {

          window.localStorage.setItem('token', result['token']);
          this.router.navigate(['/']);

        }, (error) => {
          this.errorHandler(error);
        });
    }
  }

  errorHandler(error) {
    if (error._body === '{"detail":"Invalid token."}') {
      window.localStorage.removeItem('token');
      this.router.navigate(['/login']);
    } else {
      /**
       * create sweet alert toast message
       */
    }
  }

}
