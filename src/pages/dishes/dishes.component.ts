import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';

import { OutletService } from '../../providers/outlets.service';

@Component({
  selector: './app-dishes',
  templateUrl: './dishes.component.html',
  styleUrls: ['./dishes.component.scss']
})
export class DishesComponent implements OnInit {

  public dish: any;

  constructor(
    private router: Router,
    private dishesService: OutletService,
    public sanitizer: DomSanitizer
  ) {}

  ngOnInit() {
    const url = this.router.url.split('/');
    this.dishesService.getDish(url[2])
      .then(result => {
        this.dish = result;
      }, (error) => {
        this.errorHandler(error);
      });
  }

  errorHandler(error) {
    alert('error: ' + error['status']);
  }

}
