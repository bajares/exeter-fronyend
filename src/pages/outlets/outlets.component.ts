import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { OutletService } from '../../providers/outlets.service';

@Component({
  selector: './app-outlets',
  templateUrl: './outlets.component.html',
  styleUrls: ['./outlets.component.scss']
})
export class OutletsComponent implements OnInit {

  public outlets: any;

  @Output() tab: EventEmitter<any> = new EventEmitter();

  constructor(
    private router: Router,
    private outletService: OutletService
  ) { }

  ngOnInit() {
    this.outletService.getOutletList()
      .then(result => {
        this.outlets = result;
      }, (error) => {
        this.errorHandler(error);
      });
  }

  onViewOutlet(outlet) {
    console.log(outlet);
    this.router.navigate(['outlet/' + outlet + '/']);
  }

  errorHandler(error) {
    alert('error: ' + error['status']);
  }

}
