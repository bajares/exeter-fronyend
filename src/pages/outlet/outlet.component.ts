import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { OutletService } from '../../providers/outlets.service';

@Component({
  selector: './app-outlet',
  templateUrl: './outlet.component.html',
  styleUrls: ['./outlet.component.scss']
})
export class OutletComponent implements OnInit {

  @Input() outlet: any;
  @Output() tab: EventEmitter<any> = new EventEmitter();

  constructor(
    private router: Router,
    private outletService: OutletService
  ) {}

  ngOnInit() {
    const url = this.router.url.split('/');
    this.outletService.getOutlet(url[2])
      .then(result => {
        this.outlet = result;
      }, (error) => {
        this.errorHandler(error);
      });
  }

  onViewDish(dish) {
    this.router.navigate(['dish/' + dish]);
  }

  errorHandler(error) {
    alert('error: ' + error['status']);
  }

}
