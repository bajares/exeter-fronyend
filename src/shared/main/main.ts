import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: './app-main',
  templateUrl: './main.html',
  styleUrls: ['./main.scss']
})
export class MainComponent implements OnInit {

  constructor(
    private router: Router
  ) {
  }

  ngOnInit() {
    this.checkSesion();
  }

  /**
   * CheckSesion
   */
  checkSesion() {
    if (!window.localStorage.getItem('token')) {
      this.router.navigate(['login']);
    }

  }

}
