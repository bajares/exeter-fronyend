
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { OutletService } from '../../providers/outlets.service';
import { Router } from '@angular/router';
import { AuthService } from '../../providers/auth.service';

@Component({
  selector: './app-sidebar',
  templateUrl: './sidebar.html',
  styleUrls: ['./sidebar.scss']
})
export class SidebarComponent implements OnInit {

  public client = 'isHotel';
  public user = 'Guest';
  public today: number = Date.now();

  @Output() tab: EventEmitter<any> = new EventEmitter();

  constructor(
    private router: Router,
    private sidebarService: OutletService,
    private authService: AuthService
  ) {
  }

  ngOnInit() {
    this.sidebarService.getSidebar()
      .then(result => {
        this.client = result['hotel']['name'];
        this.user = result['user']['first_name'] + ' ' + result['user']['last_name'];
      }, (error) => {
        this.errorHandler(error);
      });
  }

  /**
   * OnView
   * functions
   */
  onViewOutletList() {
    this.router.navigate(['/']);
  }

  /**
   * Logout
   */
  onLogout() {
    this.authService.logout()
    .then(result => {
      window.localStorage.removeItem('token');
      this.router.navigate(['login']);
    }, (error) => {
      this.errorHandler(error);
    });
  }

  errorHandler(error) {
    alert('error: ' + error['status']);
  }

}
