import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { global } from '../app/global';

@Injectable({
  providedIn: 'root'
})

export class OutletService {

  private headers = new HttpHeaders({
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'Authorization': 'Token ' + window.localStorage.getItem('token')
  });

  constructor(
    private http: HttpClient
  ) { }

  getSidebar() {
    return this.http.get(global.API_SIDEBAR, { headers: this.headers })
      .toPromise();
  }

  getOutletList() {
    return this.http.get(global.API_OUTLET_LIST, { headers: this.headers })
      .toPromise();
  }

  getOutlet(id) {
    const url = global.API_OUTLET.replace('__id__', id);
    return this.http.get(url, { headers: this.headers })
      .toPromise();
  }

  getDish(id) {
    const url = global.API_DISH.replace('__id__', id);
    return this.http.get(url, { headers: this.headers })
      .toPromise();
  }

}
