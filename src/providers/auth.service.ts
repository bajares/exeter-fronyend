import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { global } from '../app/global';

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  private headers = new HttpHeaders({
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  });

  private logheaders = new HttpHeaders({
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'Authorization': 'Token ' + window.localStorage.getItem('token')
  });

  constructor(
    private http: HttpClient
  ) { }

  signin(form) {
    const params = {
      email: form.value['email'],
      password: form.value['password']
    };

    return this.http.post(global.API_SIGNIN, params, { headers: this.headers })
      .toPromise();
  }

  logout() {

    return this.http.get(global.API_LOGOUT, { headers: this.logheaders })
      .toPromise();
  }

}
