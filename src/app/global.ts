import { environment } from '../environments/environment';

/*
    Server URL:
    To change the URL you can overwrite the variable
    "server_name" or directly in the environment.ts file
    located in the folder of the same name.
    ** Do not commit server url changes if not needed **
*/

const server = environment.apiUrl;

export const global = {

    /**
     * LOGIN
     * @method post
     * @returns user token
     */
    API_SIGNIN: server + 'login/',

    /**
     * LOGOUT
     * @method get
     * @returns user token
     */
    API_LOGOUT: server + 'logout/',


    /**
     * LOGIN
     * @method get
     * @returns sidebar data
     */
    API_SIDEBAR: server,

    /**
     * OUTLET LIST
     * @method get
     * @returns a list of restaurants
     * Includes: names and costs
     * Result:
     */
    API_OUTLET_LIST: server + 'outlets/',

    /**
     * OUTLET
     * @method get
     * @returns a restaurant detail
     * Includes: name, menu, costs...
     */
    API_OUTLET: server + 'outlet/__id__/',

    /**
     * DISH
     * @method get
     * @returns a dish recipe
     * Includes: ingredient, cost, unit, count...
     */
    API_DISH: server + 'food/recipe/__id__/',

    /**
     * SEARCH URL
     * @method post
     * @returns a list of dishes
     */
    API_SEARCHBAR: server + 'exeter/search/',

};
