import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';

import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/**
 * shared components
 */
import { MainComponent } from '../shared/main/main';
import { SidebarComponent } from '../shared/sidebar/sidebar';
import { HeaderComponent } from '../shared/header/header';
import { BodyComponent } from '../shared/body/body';
import { FooterComponent } from '../shared/footer/footer';

/**
 * page components
 */
import { LoginComponent } from '../pages/login/login.component';
import { OutletComponent } from '../pages/outlet/outlet.component';
import { OutletsComponent } from '../pages/outlets/outlets.component';
import { DishesComponent } from '../pages/dishes/dishes.component';

/**
 * services
 */
import { OutletService } from '../providers/outlets.service';
import { AuthService } from '../providers/auth.service';

/**
 * URLS
 */
const appRoutes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: '',
        component: OutletsComponent,
      },
      {
        path: 'outlet/:id',
        component: OutletComponent,
      },
      {
        path: 'dish/:id',
        component: DishesComponent,
      }
    ]
  },
  { path: 'login', component: LoginComponent },
];

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

registerLocaleData(localeEs);

@NgModule({
  declarations: [
    AppComponent,
    /* shared */
    MainComponent,
    SidebarComponent,
    HeaderComponent,
    BodyComponent,
    FooterComponent,
    /* pages */
    LoginComponent,
    OutletsComponent,
    OutletComponent,
    DishesComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    ),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    OutletService,
    AuthService,
    { provide: LOCALE_ID, useValue: 'es' },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
