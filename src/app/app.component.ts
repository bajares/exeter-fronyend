import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {

  constructor(
    private translate: TranslateService) {

      translate.setDefaultLang('es');
      // translate.addLangs(['en', 'es']);
      // const browserlang = translate.getBrowserLang();
      // translate.use(browserlang.match(/en|es/) ? browserlang : 'es');


  }

}
